const students = [
  { id: "SV001", name: "Nguyen Van A" },
  { id: "SV002", name: "Le Thi B" },
  { id: "SV003", name: "Tran Van C" },
];

function loadStudents() {
  const table = document.getElementById("studentTable");
  students.forEach((student) => {
    const row = document.createElement("tr");

    const idCell = document.createElement("td");
    idCell.textContent = student.id;
    row.appendChild(idCell);

    const nameCell = document.createElement("td");
    nameCell.textContent = student.name;
    row.appendChild(nameCell);

    const scoreCell = document.createElement("td");
    const scoreInput = document.createElement("input");
    scoreInput.type = "number";
    scoreInput.min = 0;
    scoreInput.max = 10;
    scoreInput.id = `score-${student.id}`;
    scoreCell.appendChild(scoreInput);
    row.appendChild(scoreCell);

    table.appendChild(row);
  });
}

function submitScores() {
  students.forEach((student) => {
    const score = document.getElementById(`score-${student.id}`).value;
    console.log(
      `Mã sinh viên: ${student.id}, Tên: ${student.name}, Điểm số: ${score}`
    );
  });
  alert("Điểm đã được lưu");
}

window.onload = loadStudents;
